import javax.imageio.ImageIO;
import javax.print.PrintService;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.OrientationRequested;
import javax.print.attribute.standard.PrinterResolution;
import javax.print.attribute.standard.QueuedJobCount;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.print.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;

import static java.nio.file.StandardWatchEventKinds.*;

public class PrintWatcher {

    public static BufferedImage rotateClockwise90(BufferedImage src) {
        int width = src.getWidth();
        int height = src.getHeight();

        BufferedImage dest = new BufferedImage(height, width, src.getType());

        Graphics2D graphics2D = dest.createGraphics();
        graphics2D.translate((height - width) / 2, (height - width) / 2);
        graphics2D.rotate(Math.PI / 2, height / 2, width / 2);
        graphics2D.drawRenderedImage(src, null);

        return dest;
    }

    public static BufferedImage toBufferedImage(Image img) {
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();
        return bimage;
    }

    public static void listFilesForFolder(final File folder) {
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry);
            } else {
                System.out.println(fileEntry.getName());
            }
        }
    }

    static public void main(String args[]) {
        String sourceFolder = args[0];
        String printerName = args[1];
        final File[] outputFolder = {null};
        if (args.length > 2) {
            outputFolder[0] = new File(args[2]);
        }
        if (outputFolder[0] == null) {
            outputFolder[0] = new File(sourceFolder + "/printed");
        }
        if (!outputFolder[0].exists()) {
            outputFolder[0].mkdir();
        }
        (new Thread(new Runnable() {
            @Override
            public void run() {
                for (;;) {
                    System.out.println("checking for new files to be printed...");
                    for (final File fileEntry : new File(sourceFolder).listFiles()) {
                        String ext = getExtension(fileEntry);
                        if (ext == null || (!ext.toLowerCase().equals("jpg") && !ext.toLowerCase().equals("jpeg"))) {
                            continue;
                        }
                        System.out.println("printing " + fileEntry.getName());
                        boolean printStarted = printFile(fileEntry, printerName);
                        if (printStarted) {
                            File printedFile = new File(outputFolder[0].getAbsolutePath() + "/" + fileEntry.getName());
                            fileEntry.renameTo(printedFile);
                        } else {
                            System.out.println("not printing yet: " + fileEntry.getName());
                        }
                    }
                    try {
                        Thread.sleep(10*1000);
                    } catch (InterruptedException e) {
                    }
                }
            }
        })).start();
        //printFile(new File("test.jpg"), "Canon SELPHY CP800");
    }

    private static String getExtension(File fileEntry) {
        int i = fileEntry.getName().lastIndexOf('.');
        if (i > 0) {
            return fileEntry.getName().substring(i+1);
        }
        return null;
    }

    private static boolean printFile(File file, String printerName) {
        try {
            Image img = ImageIO.read(file);
            boolean imageLandscape = img.getWidth(null) > img.getHeight(null);
            PrintService[] printServices = PrinterJob.lookupPrintServices();
            PrinterJob printJob = PrinterJob.getPrinterJob();
            PrintService psToUse = null;
            for (PrintService ps:printServices) {
                if (ps.getName().equals(printerName)) {
                    psToUse = ps;
                }
            }
            if (psToUse == null) {
                throw new RuntimeException("Printer not found");
            }
            if (psToUse.getAttribute(QueuedJobCount.class).getValue() > 0) {
                return false;
            }
            printJob.setPrintService(psToUse);
            PageFormat format = printJob.getPageFormat(null);
            Paper paper = format.getPaper();
            paper.setImageableArea(0.0, 0.0, format.getPaper().getWidth(), format.getPaper().getHeight());
            format.setPaper(paper);
            printJob.setPrintable(new Printable()
            {
                public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
                    if (pageIndex != 0) {
                        return NO_SUCH_PAGE;
                    }
                    boolean pageLandscape = pageFormat.getPaper().getWidth() > pageFormat.getPaper().getHeight();
                    BufferedImage imgUse = toBufferedImage(img);
                    if (pageLandscape != imageLandscape) {
                        imgUse = rotateClockwise90(imgUse);
                    }
                    double factorX = img.getWidth(null)/pageFormat.getImageableWidth();
                    double factorY = img.getHeight(null)/pageFormat.getImageableHeight();
                    double useFactor = Math.min(factorX, factorY);
                    int useHeight = (int)(img.getHeight(null)/useFactor);
                    int useWidth = (int)(img.getWidth(null)/useFactor);
                    //graphics.drawImage(img, 0, 0, (int)(pageFormat.getPaper().getWidth()), (int)(pageFormat.getPaper().getHeight()), null);
                    graphics.drawImage(imgUse, (int)((pageFormat.getImageableWidth()-useWidth)/2), (int)((pageFormat.getImageableHeight()-useHeight)/2), useWidth, useHeight,   null);
                    return PAGE_EXISTS;
                }
            }, format);
            PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
            if (imageLandscape) {
                aset.add(OrientationRequested.LANDSCAPE);
            }
            /*PrinterResolution pr = new PrinterResolution(300, 300, PrinterResolution.DPI);
            aset.add(pr);*/
            printJob.print(aset);
            /*if (printJob.printDialog()) {
                try {
                    printJob.print(aset);
                } catch (Exception prt) {
                    System.err.println(prt.getMessage());
                }
            }*/
            //fin.close();
            return true;
        } catch (IOException | PrinterException ie) {
            ie.printStackTrace();
            return false;
        }
    }
}