const mongoose = require('mongoose');

const PictureSchema = mongoose.Schema({
    uuid: String,
}, {
    timestamps: true
});
PictureSchema.index({'uuid':1}, { unique: true });

PictureSchema.statics.findAll = function(uuid) {
    return this.find().sort({createdAt:-1}).exec();
};

PictureSchema.statics.loadForUuid = function(uuid) {
    return this.findOne({uuid:uuid}).exec();
};

module.exports = mongoose.model('picture', PictureSchema);
