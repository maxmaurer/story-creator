giphySearchDebounced = _.debounce(async (vInstance, term, apiKey) => {
    vInstance.items = [];
    var line = new ProgressBar.Line('#progressContainer', { vertical: false });
    line.animate(0.8, { duration: 5000 });
    const giphyResponse = await axios({
        method: 'get',
        url: 'https://api.giphy.com/v1/stickers/search?api_key='+apiKey+'&q='+encodeURIComponent(term)+'&limit=20&rating=g'
    });
    line.animate(1, { duration: 500 }, () => {
        line.destroy();
    });
    vInstance.items = giphyResponse.data.data;
}, 300);

function addDeleteBtn(x, y, useFactor){
    $(".deleteBtn").remove();
    var btnLeft = x-(15/useFactor);
    var btnTop = y-(15/useFactor);
    var deleteBtn = '<img src="img/trash.svg" class="deleteBtn" style="position:absolute;top:'+btnTop+'px;left:'+btnLeft+'px;cursor:pointer;width:'+(30/useFactor)+'px;height:'+(30/useFactor)+'px;"/>';
    $(".canvas-container").append(deleteBtn);
}

const LONG_SIDE = 1772;
const SHORT_SIDE = 1181;

const app = new Vue({
    el: '#app',
    data: {
        topMargin: 0,
        bottomMargin: 0,
        videoContainer: null,
        container: null,
        streaming: false,
        targetWidth: LONG_SIDE,
        targetHeight: SHORT_SIDE,
        fabricCanvas: null,
        photoCanvas: null,
        videoFactor: 0,
        width: 0,
        height: 0,
        mode:"init",
        stream: null,
        currentCameraIndex: 0,
        cameras: [],
        isFullscreen: false,
        editMode: "none",
        useFactor: 0,
        items: [],
        finalImage: null,
        giphyTerm: "",
        text: "",
        pictureData: null,
        stickers: [],
        supportsFullscreen: false,
        giphyApiKey: null,
        cameraChangeCheckInterval: null
    },
    methods: {
        save: async function() {
            this.finalImage = this.fabricCanvas.toDataURL({format:'jpeg', quality: 0.9 });
            var line = new ProgressBar.Line('#progressContainer', { vertical: false });
            line.animate(0.8, { duration: 5000 });
            const result = await axios({
                method: 'post',
                url: 'api/save',
                data: {
                    image: this.finalImage
                }
            });
            this.pictureData = result.data;
            line.animate(1, { duration: 500 }, () => {
                line.destroy();
            });
            this.editMode = 'none';
            this.mode = 'final';
        },
        download: function() {
            const a = document.createElement('a');
            a.href = this.finalImage.replace("image/png", "application/octet-stream");
            a.download = 'image.jpg';
            a.click();
        },
        drawReturn: function(w) {
            if (this.editMode === 'draw') {
                this.fabricCanvas.isDrawingMode = false;
            } else if (this.editMode === 'text' && this.fabricCanvas.getActiveObject()) {
                this.fabricCanvas.discardActiveObject();
                this.fabricCanvas.requestRenderAll();
            }
            this.editMode = 'none';
        },
        setStroke: function(w) {
            this.fabricCanvas.freeDrawingBrush.width = w;
        },
        setColor: function(c) {
            this.fabricCanvas.freeDrawingBrush.color = c;
        },
        textChanged: function(e) {
            this.text = e.target.value;
            const t = this.fabricCanvas.getActiveObject();
            t.set('text', e.target.value);
            this.fabricCanvas.requestRenderAll();
            addDeleteBtn(t.oCoords.tr.x, t.oCoords.tr.y, this.useFactor);
        },
        setTextColor: function(c) {
            const o = this.fabricCanvas.getActiveObject();
            if (!o) return;
            if (o.type==='text') {
                o.set('fill', c);
                this.fabricCanvas.requestRenderAll();
            }
        },
        fullscreen: function() {
            if (!this.supportsFullscreen) return;
            if (document.documentElement.webkitRequestFullscreen) {
                document.documentElement.webkitRequestFullscreen();
            } else {
                document.documentElement.requestFullscreen();
            }
        },
        setHandlerOptions: function(element) {
            element.setControlsVisibility({
                bl: false,
                tr: false,
                mt: false,
                ml: false,
                mr: false,
                mb: false
            });
            element.cornerSize = Math.max(Math.round(30 / this.useFactor), 10);
            element.hasBorders = false;
            element.transparentCorners = false;
            element.cornerStyle = "circle";
            element.rotatingPointOffset = 5;
            element.cornerColor = "#604d93";
        },
        start: function() {
            this.fullscreen();
            this.mode = 'takePicture';
        },
        drawMode: function() {
            this.fabricCanvas.isDrawingMode = true;
            this.fabricCanvas.freeDrawingBrush.color = "#C8553D";
            this.fabricCanvas.freeDrawingBrush.width = 5;
            this.editMode = "draw";
        },
        addSticker: function(item) {
            if (item.items) {
                this.items = item.items;
            } else {
                var imgInstance = new fabric.Image.fromURL('img/sticker/' + item.imageUrl, (i) => {
                    this.fabricCanvas.add(i);
                    this.drawReturn();
                });
            }
        },
        textMode: async function() {
            var myfont = new FontFaceObserver("Roboto")
            await myfont.load();
            const t = new fabric.Text('Text', {
                left: 300,
                top: 300,
                fontSize: 300,
                fontFamily: "Roboto",
                fill: "#C8553D"
            });
            this.fabricCanvas.add(t);
            this.fabricCanvas.setActiveObject(t);
        },
        setFamily: async function(fontName) {
            var myfont = new FontFaceObserver(fontName)
            await myfont.load();
            this.fabricCanvas.getActiveObject().set('fontFamily', fontName);
            this.fabricCanvas.requestRenderAll();
        },
        addGiphy: function(item) {
            var imgInstance = new fabric.Image.fromURL(item.images.downsized_still.url, (i) => {
                this.fabricCanvas.add(i);
                this.drawReturn();
            }, {
                crossOrigin: 'anonymous'
            });
        },
        stickerMode: function() {
            this.editMode = "sticker";
            this.items = this.settings.stickers;
        },
        print: async function() {
            var line = new ProgressBar.Line('#progressContainer', { vertical: false });
            line.animate(0.8, { duration: 5000 });
            await axios({
                method: 'post',
                url: 'api/print',
                data: {
                    uuid: this.pictureData.uuid
                }
            });
            toastr.success('Datei zum Drucker geschickt!');
            line.animate(1, { duration: 500 }, () => {
                line.destroy();
            });
        },
        giphyMode: async function() {
            this.giphyTerm = "";
            this.editMode = "giphy";
            this.items = [];
            var line = new ProgressBar.Line('#progressContainer', { vertical: false });
            line.animate(0.8, { duration: 5000 });
            const giphyResponse = await axios({
                method: 'get',
                url: 'https://api.giphy.com/v1/stickers/trending?api_key='+this.settings.giphyApiKey+'&limit=30&rating=g'
            });
            line.animate(1, { duration: 500 }, () => {
                line.destroy();
            });
            this.items = giphyResponse.data.data;
        },
        takePhoto: function() {
            this.photoCanvas.width = this.targetWidth;
            this.photoCanvas.height = this.targetHeight;
            this.initFabric();
            var context = this.photoCanvas.getContext('2d');
            context.drawImage(this.videoContainer, (this.videoContainer.width - this.targetWidth)/2/this.videoFactor, (this.videoContainer.height - this.targetHeight)/2/this.videoFactor, this.targetWidth/this.videoFactor, this.targetHeight/this.videoFactor, 0, 0, this.targetWidth, this.targetHeight);
            this.stream.getTracks().forEach(track => track.stop());
            var imgInstance = new fabric.Image(this.photoCanvas);
            this.fabricCanvas.add(imgInstance);
            this.mode="edit";
            clearInterval(this.cameraChangeCheckInterval);
        },
        resize: function() {
            if (['init', 'wait', 'takePicture'].indexOf(this.mode) >= 0) {
                if (document.documentElement.clientWidth > document.documentElement.clientHeight) {
                    this.targetWidth = LONG_SIDE;
                    this.targetHeight = SHORT_SIDE;
                } else {
                    this.targetWidth = SHORT_SIDE;
                    this.targetHeight = LONG_SIDE;
                }
            }
            const xFactor = this.targetWidth / this.videoContainer.videoWidth;
            const yFactor = this.targetHeight / this.videoContainer.videoHeight;
            this.videoFactor = xFactor>yFactor?xFactor:yFactor;
            this.width = this.videoContainer.videoWidth * this.videoFactor;
            this.height = this.videoContainer.videoHeight * this.videoFactor;
            this.videoContainer.setAttribute('width', this.width);
            this.videoContainer.setAttribute('height', this.height);
            if (this.cameras[this.currentCameraIndex].label.toLocaleLowerCase().indexOf("front") !== -1) {
                this.videoContainer.style.transform = 'scale(-1, 1)';
            } else {
                this.videoContainer.style.transform = '';
            }
            const xFactorWindow = document.documentElement.clientWidth / this.targetWidth;
            const yFactorWindow = document.documentElement.clientHeight / this.targetHeight;
            this.useFactor = xFactorWindow<yFactorWindow?xFactorWindow:yFactorWindow;
            this.container.style.transform = "scale(" + this.useFactor + ")";
            this.container.style.left = ((document.documentElement.clientWidth - this.targetWidth * this.useFactor) / 2 - (this.targetWidth - this.targetWidth * this.useFactor) / 2) + "px";
            this.container.style.top = ((document.documentElement.clientHeight - this.targetHeight * this.useFactor) / 2 - (this.targetHeight - this.targetHeight * this.useFactor) / 2) + "px";
        },
        nextCamera: async function() {
            if (this.stream) {
                this.stream.getTracks().forEach(track => track.stop());
                this.streaming = false;
            }
            this.currentCameraIndex++;
            this.currentCameraIndex %= this.cameras.length;

            this.stream = await navigator.mediaDevices.getUserMedia({video: {
                deviceId: this.cameras[this.currentCameraIndex].deviceId,
                    width: 1920
                }, audio: false});
            this.videoContainer.srcObject = this.stream;
            this.videoContainer.play();
        },
        giphyTermChanged: function(e) {
            this.giphyTerm = e.target.value;
            giphySearchDebounced(this, e.target.value, this.settings.giphyApiKey);
        },
        initFabric: function() {
            this.fabricCanvas = new fabric.Canvas('fabricCanvas', {
                selection: false,
                enableRetinaScaling: false
            });
            this.fabricCanvas.preserveObjectStacking = true;
            this.fabricCanvas.on('changed', (e) => {
                addDeleteBtn(e.target.oCoords.tr.x, e.target.oCoords.tr.y, this.useFactor);
            });

            this.fabricCanvas.on('selection:updated',(e) =>{
                this.setHandlerOptions(e.target);
                addDeleteBtn(e.target.oCoords.tr.x, e.target.oCoords.tr.y, this.useFactor);
                if (e.target.type === 'text') {
                    this.editMode = 'text';
                    this.text = e.target.text;
                } else if (e.target.type !== 'text') {
                    this.drawReturn();
                }
            });

            this.fabricCanvas.on('selection:cleared', (e) => {
               if (this.editMode === 'text') {
                   this.drawReturn();
               }
                $(".deleteBtn").remove();
            });

            this.fabricCanvas.on('selection:created',(e) =>{
                this.setHandlerOptions(e.target);
                addDeleteBtn(e.target.oCoords.tr.x, e.target.oCoords.tr.y, this.useFactor);
                if (e.target.type === 'text') {
                    this.editMode = 'text';
                    this.text = e.target.text;
                }
            });

            this.fabricCanvas.on('mouse:down',(e) => {
                if(!this.fabricCanvas.getActiveObject()) {
                    $(".deleteBtn").remove();
                }
            });

            this.fabricCanvas.on('object:modified',(e) => {
                addDeleteBtn(e.target.oCoords.tr.x, e.target.oCoords.tr.y, this.useFactor);
            });

            this.fabricCanvas.on('object:scaling',(e) => {
                $(".deleteBtn").remove();
            });
            this.fabricCanvas.on('object:moving',(e) => {
                $(".deleteBtn").remove();
            });
            this.fabricCanvas.on('object:rotating',(e) =>{
                $(".deleteBtn").remove();
            });
            $(document).on('click',".deleteBtn",() => {
                if(this.fabricCanvas.getActiveObject()) {
                    this.fabricCanvas.remove(this.fabricCanvas.getActiveObject());
                    $(".deleteBtn").remove();
                }
            });
        }
    },
    created() {
        window.addEventListener("resize", this.resize);
        window.addEventListener("fullscreenchange", () => {
            this.isFullscreen = document.webkitIsFullScreen || document.mozFullScreen;
        });
        window.addEventListener("webkitfullscreenchange", () => {
            this.isFullscreen = document.webkitIsFullScreen || document.mozFullScreen;
        });
    },
    destroyed() {
        window.removeEventListener("resize", this.resize);
    },
    mounted: async function() {
        const ua = window.navigator.userAgent;
        const iOS = !!ua.match(/iPad/i) || !!ua.match(/iPhone/i);
        const webkit = !!ua.match(/WebKit/i);
        const iOSSafari = iOS && webkit && !ua.match(/CriOS/i);
        if (iOSSafari) {
            document.addEventListener('touchmove', function (event) {
                if (event.scale !== 1) { event.preventDefault(); }
            }, false);
        }
        this.supportsFullscreen = !iOSSafari;
        this.videoContainer = document.getElementById('video');
        this.container = document.getElementById('container');
        this.photoCanvas = document.getElementById('photoCanvas');
        this.cameras = (await navigator.mediaDevices.enumerateDevices()).filter(c => c.kind==='videoinput');
        console.log(this.cameras);
        this.cameraChangeCheckInterval = setInterval(async () => {
            this.cameras = (await navigator.mediaDevices.enumerateDevices()).filter(c => c.kind==='videoinput');
            console.log(this.cameras);
        }, 10000);
        this.currentCameraIndex = 0;
        this.stream = await navigator.mediaDevices.getUserMedia({video: {
            deviceId: this.cameras[0].deviceId,
                width: 1920
            }, audio: false});
        this.videoContainer.srcObject = this.stream;
        this.videoContainer.play();
        this.videoContainer.addEventListener('canplay', (ev) => {
            if (!this.streaming) {
                this.resize();
                streaming = true;
            }
            if (this.mode === 'init') {
                this.mode = 'wait';
            }
        }, false);
        this.settings = (await axios({
            method: 'get',
            url: '/api/settings'
        })).data;
    }
});