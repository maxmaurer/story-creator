const app = new Vue({
    el: '#app',
    data: {
        images: [],
        printImages: []
    },
    methods: {
        getImages: async function () {
            const result = await axios({
                method: 'get',
                url: 'api/images'
            });
            for (const i of result.data) {
                i.showPrint = false;
            }
            this.images = result.data;
        },
        getPrintImages: async function () {
            const result = await axios({
                method: 'get',
                url: '/api/printImages'
            });
            this.printImages = result.data;
        },
        showPrint: async function(image) {
            image.showPrint = !image.showPrint;
        },
        print: async function(image) {
            await axios({
                method: 'post',
                url: '/api/print',
                data: {
                    uuid: image.uuid
                }
            });
            toastr.success('Erneut zur Druckwarteschlange hinzugefügt');
        }
    },
    mounted: async function() {
        this.getImages();
        this.getPrintImages();
        setInterval(() => {
            this.getImages();
            this.getPrintImages();
        }, 30000);
    }
});