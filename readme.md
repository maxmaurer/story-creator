# Story Creator

This project is meant to create an instagram like story creator for the web.

It makes use of

* Node.JS
* vueJS
* webRTC
* canvas
* fabric.js
* Giphy
* Dropbox

## Getting started
* check out the code
* do a yarn install
* add your own set of env variables
* run index.js
* access `http://localhost:5001`

## Current Main Issues
* not fully running on current iOS mobile

## Environment Variables

| Name | Value |
|------|-------|
| ADMIN_PASSWORD | The password for accessing the gallery. User name will be "admin". |
| AWS_ACCESS_KEY_ID | An aws access key that allows access to a bucket for storing the images.
| AWS_BUCKET_NAME | The aws bucket name for storing the images into |
| AWS_SECRET_ACCESS_KEY | The secret key for the aws account with the S3 bucket |
| DEBUG | how much debug information to show: use * for all or only "story-creator" for all the logs |
| DROPBOX_ACCESS_TOKEN | Enter a dropbox access token to where the images shall be stored 
| GIPHY_API_KEY | enter a key to access the giphy API |
| MONGODB | A connections string to a mongo database to store the data in |
 
