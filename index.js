const express = require('express');
const app = express();
const http = require('http').createServer(app);
const debug = require('debug')('story-creator');
const bodyParser = require('body-parser');
const fs = require('fs');
const rp = require('request-promise');
const request = require('request');
const fetch = require('isomorphic-fetch');
const uuid = require('uuid').v4;
const Dropbox = require('dropbox').Dropbox;
const moment = require('moment');
const AWS = require('aws-sdk');
const s3 = new AWS.S3();
const mongoose = require('mongoose');
const passport = require('passport');
const BasicStrategy = require('passport-http').BasicStrategy;
const Picture = require('./lib/models/picture');
mongoose.Promise = require('bluebird');
mongoose.connect(process.env.MONGODB, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

/*async function storeStatic(term) {
    const images = await rp({
        url: 'https://api.giphy.com/v1/stickers/search?api_key=SwYOgIupB9YhSkdzjdo1b2jOaeUSID78&q='+encodeURIComponent(term)+'&limit=25&rating=g',
        json: true
    });
    for (let i=0;i<images.data.length && i < 10;i++) {
        const item = images.data[i];
        request(item.images.downsized_still.url).on('response', r => {
            console.log('Response received: ' + r);
        }).pipe(fs.createWriteStream('./public/img/sticker/' + term + '_' + i + '.gif'));
    }
    debug(images);
}
storeStatic('champagne');*/

if (process.env.NODE_ENV === 'production') {
    app.use(require('./lib/service/forceSsl'));
}

passport.use(new BasicStrategy(
    function(username, password, done) {
        if (username.toLowerCase() !== 'admin') return done(null, false);
        if (password !== process.env.ADMIN_PASSWORD) return done(null, false);
        return done(null, 'admin');
    }
));

app.use(bodyParser.json({limit: '50mb'}))
app.use('/', express.static('public'));
app.get('/', (req, res) => {
    res.render('editor.pug');
});
app.get('/gallery', passport.authenticate('basic', { session: false }), (req, res) => {
    res.render('gallery.pug');
});
/*async function s3Test() {
    try {
        const objects = await s3.listObjectsV2({
            Bucket: 'webstergram'
        }).promise();
        debug(objects);
    } catch (e) {
        debug(e);
    }
}
s3Test();*/
app.get('/api/images', async (req, res, next) => {
    try {
        const pictures = await Picture.findAll();
        res.send(pictures);
    } catch (e) {
        next(e);
    }
});
app.get('/api/printImages', async (req, res, next) => {
    try {
        const dbClient = new Dropbox({
            fetch: fetch,
            accessToken: process.env.DROPBOX_ACCESS_TOKEN
        });
        const files = await dbClient.filesListFolder({
            path: '/print'
        });
        const imageList = [];
        for (const f of files.entries) {
            if (f['.tag'] === 'folder') {
                continue;
            }
            imageList.push({
               uuid: f.name.substr(0, f.name.length - 4)
            });
        }
        res.send(imageList);
    } catch (e) {
        next(e);
    }
});
const stickerFiles = fs.readdirSync('./public/img/sticker').sort().map(f => { return { imageUrl: f} });
app.get('/api/settings', async (req, res, next) => {
    try {
        res.send({
            stickers: stickerFiles,
            giphyApiKey: process.env.GIPHY_API_KEY
        });
    } catch (e) {
        next(e);
    }
});
app.get('/api/img/:uuid', async (req, res, next) => {
    try {
        const picture = await Picture.loadForUuid(req.params.uuid);
        const obj = await s3.getObject({
            Bucket: process.env.AWS_BUCKET_NAME,
            Key: picture.uuid
        }).promise();
        res.type('image/jpeg');
        res.end(obj.Body);
    } catch (e) {
        next(e);
    }
});
app.post('/api/save', async (req, res, next) => {
    try {
        if (!req.body.image) {
            return next(new Error('no image field present'));
        }
        if (!req.body.image.startsWith('data:image/jpeg;base64,')) {
            return next(new Error('image does not have the right format'));
        }
        const base64Data = req.body.image.replace(/^data:image\/jpeg;base64,/, "");
        const dbClient = new Dropbox({
            fetch: fetch,
            accessToken: process.env.DROPBOX_ACCESS_TOKEN
        });
        const id = uuid();
        const p = new Picture();
        p.uuid = id;
        await dbClient.filesUpload({
            contents: new Buffer(base64Data, 'base64'),
            path: '/'+ moment().format('YYYYMMDD-HHmmss')+'_'+id+'.jpg',
            autorename: true
        });
        await s3.putObject({
            Body: new Buffer(base64Data, 'base64'),
            Bucket: 'webstergram',
            Key: id
        }).promise();
        await p.save();
        /*fs.writeFile("image.jpg", base64Data, 'base64', function(err) {
            console.log(err);
        });*/
        res.send(p);
    } catch (e) {
        next(e);
    }
});
app.post('/api/print', async (req, res, next) => {
    try {
        if (!req.body.uuid) {
            return next(new Error('no image uuid present'));
        }
        const p = await Picture.loadForUuid(req.body.uuid);
        const obj = await s3.getObject({
            Bucket: 'webstergram',
            Key: p.uuid
        }).promise();
        const dbClient = new Dropbox({
            fetch: fetch,
            accessToken: process.env.DROPBOX_ACCESS_TOKEN
        });
        await dbClient.filesUpload({
            contents: obj.Body,
            path: '/print/'+ p.uuid+'.jpg',
            autorename: true
        });
        res.status(204).end();
    } catch (e) {
        next(e);
    }
});
let server = http.listen(process.env.PORT || 5001, function () {
    let host = server.address().address;
    let port = server.address().port;

    console.log('Listening at http://%s:%s', host, port);
});
